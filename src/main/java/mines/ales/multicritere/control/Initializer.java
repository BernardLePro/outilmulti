package mines.ales.multicritere.control;

import mines.ales.multicritere.model.Alternative;
import mines.ales.multicritere.model.Critere;
import mines.ales.multicritere.model.Expert;
import mines.ales.multicritere.model.Intervalle;

import java.util.ArrayList;
import java.util.List;

public class Initializer {


    public static final String EVACUATE = "Evacuate";
    public static final String SHELTERING = "Sheltering";
    public static final String PHYSICAL_INJURY = "Physical injury";
    public static final String PSYCHOLOGICAL = "Psychological";
    public static final String ENVIRONMENTAL = "Environmental";
    public static final String ECONOMIC = "Economic";
    public static final String MEDIA = "Media";

    public static List<Expert> initialize() {
        List<Expert> experts = new ArrayList<Expert>();
        experts.add(getSmith());
        experts.add(getDemoran());
        experts.add(getAgrelle());
        experts.add(getZattief());
        experts.add(getKusto());
        return experts;
    }

    static Expert getSmith() {
        List<Alternative> alternatives;
        List<Critere> criteres = new ArrayList<Critere>();
        Critere phi = new Critere(PHYSICAL_INJURY);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 10, 13));
        phi.setAlternatives(alternatives);

        Critere psi = new Critere(PSYCHOLOGICAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 13, 16));
        psi.setAlternatives(alternatives);

        Critere env = new Critere(ENVIRONMENTAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        env.setAlternatives(alternatives);

        Critere eco = new Critere(ECONOMIC);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        eco.setAlternatives(alternatives);

        Critere med = new Critere(MEDIA);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 9, 10));
        alternatives.add(new Alternative(SHELTERING, 8, 11));
        med.setAlternatives(alternatives);

        criteres.add(phi);
        criteres.add(psi);
        criteres.add(env);
        criteres.add(eco);
        criteres.add(med);
        return new Expert("Smith", criteres);
    }

    static Expert getDemoran() {
        List<Alternative> alternatives;
        List<Critere> criteres = new ArrayList<Critere>();
        Critere phi = new Critere(PHYSICAL_INJURY);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 12, 15));
        phi.setAlternatives(alternatives);

        Critere psi = new Critere(PSYCHOLOGICAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 7, 10));
        alternatives.add(new Alternative(SHELTERING, 13, 16));
        psi.setAlternatives(alternatives);

        Critere env = new Critere(ENVIRONMENTAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        env.setAlternatives(alternatives);

        Critere eco = new Critere(ECONOMIC);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        eco.setAlternatives(alternatives);

        Critere med = new Critere(MEDIA);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 9, 10));
        alternatives.add(new Alternative(SHELTERING, 8, 11));
        med.setAlternatives(alternatives);

        criteres.add(phi);
        criteres.add(psi);
        criteres.add(env);
        criteres.add(eco);
        criteres.add(med);
        return new Expert("Demoran", criteres);
    }

    static Expert getAgrelle() {
        List<Alternative> alternatives;
        List<Critere> criteres = new ArrayList<Critere>();
        Critere phi = new Critere(PHYSICAL_INJURY);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 5, 9));
        alternatives.add(new Alternative(SHELTERING, 12, 15));
        phi.setAlternatives(alternatives);

        Critere psi = new Critere(PSYCHOLOGICAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        psi.setAlternatives(alternatives);

        Critere env = new Critere(ENVIRONMENTAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 7));
        alternatives.add(new Alternative(SHELTERING, 13, 16));
        env.setAlternatives(alternatives);

        Critere eco = new Critere(ECONOMIC);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        eco.setAlternatives(alternatives);

        Critere med = new Critere(MEDIA);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 9, 10));
        alternatives.add(new Alternative(SHELTERING, 8, 11));
        med.setAlternatives(alternatives);

        criteres.add(phi);
        criteres.add(psi);
        criteres.add(env);
        criteres.add(eco);
        criteres.add(med);
        return new Expert("Agrelle", criteres);
    }

    static Expert getZattief() {
        List<Alternative> alternatives;
        List<Critere> criteres = new ArrayList<Critere>();
        Critere phi = new Critere(PHYSICAL_INJURY);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 8));
        alternatives.add(new Alternative(SHELTERING, 12, 15));
        phi.setAlternatives(alternatives);

        Critere psi = new Critere(PSYCHOLOGICAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 9));
        alternatives.add(new Alternative(SHELTERING, 15, 16));
        psi.setAlternatives(alternatives);

        Critere env = new Critere(ENVIRONMENTAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 13, 16));
        env.setAlternatives(alternatives);

        Critere eco = new Critere(ECONOMIC);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        eco.setAlternatives(alternatives);

        Critere med = new Critere(MEDIA);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 8, 11));
        alternatives.add(new Alternative(SHELTERING, 7, 10));
        med.setAlternatives(alternatives);

        criteres.add(phi);
        criteres.add(psi);
        criteres.add(env);
        criteres.add(eco);
        criteres.add(med);
        return new Expert("Zattief", criteres);
    }

    static Expert getKusto() {
        List<Alternative> alternatives;
        List<Critere> criteres = new ArrayList<Critere>();
        Critere phi = new Critere(PHYSICAL_INJURY);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 9, 13));
        phi.setAlternatives(alternatives);

        Critere psi = new Critere(PSYCHOLOGICAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 7, 9));
        alternatives.add(new Alternative(SHELTERING, 14, 16));
        psi.setAlternatives(alternatives);

        Critere env = new Critere(ENVIRONMENTAL);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 6, 7));
        alternatives.add(new Alternative(SHELTERING, 13, 16));
        env.setAlternatives(alternatives);

        Critere eco = new Critere(ECONOMIC);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 4, 7));
        alternatives.add(new Alternative(SHELTERING, 15, 17));
        eco.setAlternatives(alternatives);

        Critere med = new Critere(MEDIA);
        alternatives = new ArrayList<Alternative>();
        alternatives.add(new Alternative(EVACUATE, 8, 11));
        alternatives.add(new Alternative(SHELTERING, 7, 10));
        med.setAlternatives(alternatives);

        criteres.add(phi);
        criteres.add(psi);
        criteres.add(env);
        criteres.add(eco);
        criteres.add(med);
        return new Expert("Kusto", criteres);
    }
}
