package mines.ales.multicritere.control;

import au.com.bytecode.opencsv.CSVReader;
import mines.ales.multicritere.Utils;
import mines.ales.multicritere.model.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Controller {
    public static List<Preference> getPreferencesFromFile() {
        List<Preference> preferences = new ArrayList<Preference>();
        List<Critere> criteres = new ArrayList<Critere>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("preference.csv"), ';');
            String[] nextLine;
            int i = -1;
            while ((nextLine = reader.readNext()) != null) {
                if (i == -1) {
                    for (String nom : nextLine) {
                        criteres.add(new Critere(nom));
                    }
                    i++;
                } else {
                    int j = 0;
                    for (String poids : nextLine) {
                        preferences.add(new Preference(criteres.get(i), criteres.get(j), Float.parseFloat(poids)));
                        j++;
                    }
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return preferences;
    }

    public static List<Expert> getExpertFromFiles() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new File("alternatives.json"), new TypeReference<List<Expert>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static List<Alternative> getAlternatives(Critere critere, Alternative alternative) {
        List<Alternative> alternatives = new ArrayList<Alternative>();
        for (Expert expert : getExpertFromFiles()) {
            for (Critere crit : expert.getCriteres()) {
                if (crit.equals(critere)) {
                    for (Alternative alt : crit.getAlternatives()) {
                        if (alt.equals(alternative))
                            alternatives.add(alt);
                    }
                }
            }
        }
        return alternatives;
    }

    /**
     * @return Les critères et les trapèzes utlies pour calculer la distribution agrégée par rapport à l'alternative
     */
    public static Map<Critere, TrapezePi> getTrapezesForAlternative(Alternative alternative) {
        Map<Critere, TrapezePi> map = new HashMap<Critere, TrapezePi>();
        for (Critere crit : getCriteresPossibles()) {
            map.put(crit, getTrapeze(getAlternatives(crit, alternative)));
        }
        return map;
    }

    /**
     * @return la distribution agrégée pour une alternative suivant les avis des différents experts
     */
    public static TrapezePi getDistributionAgregeeForAlternative(Alternative alternative) {
        Map<Critere, TrapezePi> map = getTrapezesForAlternative(alternative);
        List<Preference> preferences = getPreferencesFromFile();
        TrapezePi trapezePiTemp = new TrapezePi(0, 0, 0, 0);
        for (Critere crit1 : map.keySet()) {
            trapezePiTemp.a += map.get(crit1).a * getMuI(preferences, crit1);
            trapezePiTemp.b += map.get(crit1).b * getMuI(preferences, crit1);
            trapezePiTemp.c += map.get(crit1).c * getMuI(preferences, crit1);
            trapezePiTemp.d += map.get(crit1).d * getMuI(preferences, crit1);
            for (Critere crit2 : getListCritereContenantPas(crit1)) {
                trapezePiTemp.a -= getIij(preferences, crit1, crit2) * Math.abs(map.get(crit1).a - map.get(crit2).a);
                trapezePiTemp.b -= getIij(preferences, crit1, crit2) * Math.abs(map.get(crit1).b - map.get(crit2).b);
                trapezePiTemp.c -= getIij(preferences, crit1, crit2) * Math.abs(map.get(crit1).c - map.get(crit2).c);
                trapezePiTemp.d -= getIij(preferences, crit1, crit2) * Math.abs(map.get(crit1).d - map.get(crit2).d);
            }
        }
        System.out.println(trapezePiTemp);
        return trapezePiTemp;
    }

    public static List<Critere> getListCritereContenantPas(Critere critere) {
        List<Critere> criteres = getCriteresPossibles();
        criteres.remove(critere);
        return criteres;
    }

    public static float getIndicateurPosition(Alternative alternative) {
        return (getEsperanceInferieure(alternative) + getEsperanceSuperieure(alternative)) / 2;
    }

    public static float getIndicateurImprecisionMoyenne(Alternative alternative) {
        return getEsperanceSuperieure(alternative) - getEsperanceInferieure(alternative);
    }

    public static float getEsperanceInferieure(Alternative alternative) {
        TrapezePi trapezePi = getDistributionAgregeeForAlternative(alternative);
        return (trapezePi.b + trapezePi.a) / 2;
    }

    public static float getEsperanceSuperieure(Alternative alternative) {
        TrapezePi trapezePi = getDistributionAgregeeForAlternative(alternative);
        return (trapezePi.d + trapezePi.c) / 2;
    }

    /**
     * @return le poids d'un critère pour l'intégrale de Choquet
     */
    public static float getMuI(List<Preference> preferences, Critere critere) {
        for (Preference preference : preferences) {
            if (preference.getCritere1().equals(critere) && preference.getCritere2().equals(critere))
                return preference.getPoids();
        }
        return 0;
    }

    public static void getMeilleureAlternative() {
        for (Alternative alternative : getAlternativesPossibles()) {
            System.out.println(alternative.getNom());
            System.out.println("Imprécision:" + getIndicateurImprecisionMoyenne(alternative));
            System.out.println("Position:" + getIndicateurPosition(alternative));
        }
    }

    /**
     * @return l'interaction entre les critères pour l'intégrale de Choquet
     */
    public static float getIij(List<Preference> preferences, Critere critere1, Critere critere2) {
        for (Preference preference : preferences) {
            if (preference.getCritere1().equals(critere1) && preference.getCritere2().equals(critere2))
                return preference.getPoids();
        }
        return 0;
    }

    public static List<Alternative> getAlternativesPossibles() {
        return getExpertFromFiles().get(0).getCriteres().get(0).getAlternatives();
    }

    public static List<Critere> getCriteresPossibles() {
        return getExpertFromFiles().get(0).getCriteres();
    }

    public static TrapezePi getTrapeze(List<Alternative> alternatives) {
        List<Intervalle> intervalles = new ArrayList<Intervalle>();
        for (Alternative alternative : alternatives) {
            intervalles.add(alternative.getIntervalle());
        }
        return Utils.getTrapeze(intervalles);
    }
}
