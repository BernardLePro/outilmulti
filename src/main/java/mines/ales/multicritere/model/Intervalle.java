package mines.ales.multicritere.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties
public class Intervalle {
    private int valeurMin;
    private int valeurMax;
    public Intervalle(){}

    public Intervalle(int valeurMin, int valeurMax) {
        this.valeurMin = valeurMin;
        this.valeurMax = valeurMax;
    }

    public int getValeurMin() {
        return valeurMin;
    }

    public int getValeurMax() {
        return valeurMax;
    }

    public void setValeurMin(int valeurMin) {
        this.valeurMin = valeurMin;
    }

    public void setValeurMax(int valeurMax) {
        this.valeurMax = valeurMax;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Intervalle{");
        sb.append("valeurMin=").append(valeurMin);
        sb.append(", valeurMax=").append(valeurMax);
        sb.append('}');
        return sb.toString();
    }
}
