package mines.ales.multicritere.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties
public class Critere implements Comparable {
    private String nom;
    private List<Alternative> alternatives;

    public Critere() {
    }

    public Critere(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Critere{");
        sb.append("nom='").append(nom).append('\'');
        if (alternatives != null)
            sb.append(", alternatives=").append(alternatives);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Critere)
            return getNom().compareTo(((Critere) o).getNom());
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Critere critere = (Critere) o;

        if (!nom.equals(critere.nom)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return nom.hashCode();
    }
}
