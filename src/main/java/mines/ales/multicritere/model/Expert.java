package mines.ales.multicritere.model;

import java.util.List;

public class Expert {
    private String nom;
    private List<Critere> criteres;

    public Expert() {
    }

    public Expert(String nom, List<Critere> criteres) {
        this.nom = nom;
        this.criteres = criteres;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Critere> getCriteres() {
        return criteres;
    }

    public void setCriteres(List<Critere> criteres) {
        this.criteres = criteres;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Expert{");
        sb.append("nom='").append(nom).append('\'');
        sb.append(", criteres=").append(criteres);
        sb.append('}');
        return sb.toString();
    }
}
