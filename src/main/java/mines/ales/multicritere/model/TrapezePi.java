package mines.ales.multicritere.model;

public class TrapezePi {
    public float a, b, c, d;

    public TrapezePi(float a, float b, float c, float d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public String toString() {
        return "TrapezePi{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", d=" + d +
                '}';
    }
}
