package mines.ales.multicritere.model;

public class Preference {
    private Critere critere1;
    private Critere critere2;
    private float poids;

    /**
     *
     * @param critere1
     * @param critere2
     * @param poids poids ou interactions du critere1/critere2
     */
    public Preference(Critere critere1, Critere critere2, float poids) {
        this.critere1 = critere1;
        this.critere2 = critere2;
        this.poids = poids;
    }

    public Critere getCritere1() {
        return critere1;
    }

    public Critere getCritere2() {
        return critere2;
    }

    public float getPoids() {
        return poids;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "critere1=" + critere1 +
                ", critere2=" + critere2 +
                ", poids=" + poids +
                '}';
    }
}
