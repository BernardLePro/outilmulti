package mines.ales.multicritere.model;

public class Alternative implements Comparable {
    private String nom;
    private Intervalle intervalle;

    public Alternative() {
    }

    public Alternative(String nom, Intervalle intervalle) {
        this.nom = nom;
        this.intervalle = intervalle;
    }

    public Alternative(String nom, int valeurMin, int valeurMax) {
        this.nom = nom;
        this.intervalle = new Intervalle(valeurMin, valeurMax);
    }

    public String getNom() {
        return nom;
    }

    public Intervalle getIntervalle() {
        return intervalle;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setIntervalle(Intervalle intervalle) {
        this.intervalle = intervalle;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Alternative{");
        sb.append("nom='").append(nom).append('\'');
        sb.append(", intervalle=").append(intervalle);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Alternative)
            return getNom().compareTo(((Alternative) o).getNom());
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alternative that = (Alternative) o;

        if (!nom.equals(that.nom)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return nom.hashCode();
    }
}

