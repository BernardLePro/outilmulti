package mines.ales.multicritere;

import mines.ales.multicritere.model.Intervalle;
import mines.ales.multicritere.model.TrapezePi;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Utils {

    public static TrapezePi getTrapeze(List<Intervalle> intervalles) {
        List<Integer> valeursMin = new ArrayList<Integer>();
        List<Integer> valeursMax = new ArrayList<Integer>();
        int a, b, c, d;
        for (Intervalle intervalle : intervalles) {
            valeursMin.add(intervalle.getValeurMin());
            valeursMax.add(intervalle.getValeurMax());
        }
        Collections.sort(valeursMin);
        Collections.sort(valeursMax);
        a = valeursMin.get(0);
        b = valeursMin.get(valeursMin.size() - 1);
        c = valeursMax.get(0);
        d = valeursMax.get(valeursMax.size() - 1);
        return new TrapezePi(a, b, c, d);
    }

    void test() {
        ObjectMapper objectMapper;
    }
}
