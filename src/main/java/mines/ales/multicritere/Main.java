package mines.ales.multicritere;

import mines.ales.multicritere.control.Controller;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Controller.getMeilleureAlternative();
    }
    /**
     * Commentaire général:
     * Table 2 sur la diagonale: c'est le poids des différents critéres
     *
     *  Intégrale de Choquet nous donne les 4 points seulement s'il n'y pas d'intersections, s'il y a intersection alors
     * il y a plus de points
     *
     * Les E_*(Pi) et E^*(Pi) servent à calculer le MD: on compare ensuite les MD pour connaître quelles distribution
     * est la meilleure (a le plus grand MD)
     *
     * L'indicateur de précision permet de calculer
     */
}
